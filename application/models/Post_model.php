<?php
class Post_model extends CI_Model {
    
    public function postupload($data)
	{   
        //return $this->db->get('post');
        $this->db->insert('post',$data);
        $cid = $this->givecid();
        $this->db->where(array("c_id" =>  $cid[0]['c_id']));
        
    }
    public function get_credentials($id){
        $this->db->select("*");
        $this->db->from("credentials");
        $this->db->where("c_id",$id);
        $row = $this->db->get();
        return $row->result_array();
    }
    public function givecid(){
        $this->db->select("c_id");
        $this->db->from("credentials");
        $this->db->where('email',$this->session->email);
        $row = $this->db->get();
        return $row->result_array();
    }
    public function dashposts(){
        $cid=$this->givecid();
        $this->db->select('c.*,p.*');
        $this->db->from('credentials c');
        $this->db->join('post p', 'c.c_id = p.c_id','inner');
        $row = $this->db->get();
        return $row->result_array();
    }
    public function chat_cid_handler($id){
        $cid=$this->givecid();
        $this->db->select('c_id');
        $this->db->from('post');
        $this->db->where('post_id',$id);
        $row = $this->db->get();
        $from_cid = $row->result_array();
        $cid_array = array("from_cid"   => $from_cid[0],"your_cid"    => $cid[0]);
        return $cid_array;
    }
    public function getmessage_from_post($data){
        $sql = "SELECT * FROM chat where (from_user_cid = ? AND to_user_cid = ?) or (from_user_cid = ? AND to_user_cid = ?)";
        $rows = $this->db->query($sql, array($data['from_cid']['c_id'],$data['your_cid']['c_id'],$data['your_cid']['c_id'],$data['from_cid']['c_id']));
        return $rows->result_array();
    }
    public function getmessage_from_nav($data){
        $this->db->select('*');
        $this->db->from('chat');
        $this->db->where("from_user_cid =",$data);
        $this->db->or_where("to_user_cid =",$data);
        $rows = $this->db->get();
        return $rows->result_array();
    }
    public function get_cid_from_mail($mail){
        $this->db->select("c_id");
        $this->db->from("credentials");
        $this->db->where("email =",$mail);
        $rows = $this->db->get();
        return $rows->result_array();
    }
    public function open_chat_for_nav($to_cid){
        $loggedin_cid = $this->givecid();
        $sql = "SELECT * FROM chat where (from_user_cid = ? AND to_user_cid = ?) or (from_user_cid = ? AND to_user_cid = ?)";
        $rows = $this->db->query($sql, array($loggedin_cid[0]['c_id'],$to_cid,$to_cid,$loggedin_cid[0]['c_id']));
        return $rows->result_array();
    }
    public function seen_change($id){
        $change = array("seen" => 1);
        $this->db->update('chat',$change,array('chat_id' => $id));
    }
    public function sendmessage($data){
        $this->db->insert('chat',$data);
    }
    public function viewposts4ja(){
        $cid=$this->givecid();
        $this->db->select('c.*,p.*');
        $this->db->from('credentials c');
        $this->db->join('post p', 'c.c_id = p.c_id','inner');
        $this->db->where('p.c_id',$cid[0]['c_id']);
        //$row = $this->db->get('post');
        $row = $this->db->get();
        return $row->result_array();
    }
    
    public function postedit($data,$id){
        $this->db->update('post', $data, array('post_id' =>  $id));
    }
    public function deletepost($id){
        $this->db->delete('post',array('post_id'=> $id));
    }
    
    public function detailpost($id){
        
        $this->db->select('c.*,p.*');
        $this->db->from('credentials c');
        $this->db->join('post p', 'c.c_id = p.c_id','inner');
        $this->db->where(array("p.post_id"=>$id));
        
        //$row = $this->db->get('post');
        $row = $this->db->get();
        return $row->result_array();
    }
    public function pro_check(){
        $cid = $this->givecid();
        $this->db->select("pro");
        $this->db->from("credentials");
        $this->db->where("c_id",$cid[0]['c_id']);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function applyit($data){
        $this->db->insert('apply',$data);
    }
    public function checkit($data){
        $this->db->select("*");
        $this->db->from("apply");
        $this->db->where("c_id", $data['c_id']);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function post_responses($cid){
        $this->db->select("a.c_id");
        $this->db->from("post p");
        $this->db->join("apply a","p.post_id = a.post_id","inner");
        $this->db->where("p.c_id", $cid);
        $row = $this->db->get();
        return $row->result_array();
    }
    public function get_post(){
        $this->db->select("p.*,c.*");
        $this->db->from("post p");
        $this->db->join("credentials c", "p.c_id = c.c_id","inner");
        $this->db->where("p.approved","0");
        $this->db->where("p.disabled", "0");
        $row = $this->db->get();
        return $row->result_array();
    }
    public function get_post_upload_post($id){
        $this->db->select("p.*,c.*");
        $this->db->from("post p");
        $this->db->join("credentials c", "p.c_id = c.c_id","inner");
        $this->db->where("c.c_id",$id);
        $row = $this->db->get();
        return $row->result_array();
    }
}
