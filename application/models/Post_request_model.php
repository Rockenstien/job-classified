<?php
class Post_request_model extends CI_MODEL{
    public function request_posts(){
        $cid=$this->Post_model->givecid();
        $this->db->select('c.*,p.*');
        $this->db->from('credentials c');
        $this->db->join('post p', 'c.c_id = p.c_id','inner');
        $this->db->where('p.approved', 0);
        //$row = $this->db->get('post');
        $row = $this->db->get();
        return $row->result_array();
    }
    public function approve($id){
        $this->db->update('post', array('approved'  =>  "1"), array('post_id' =>  $id));
    }
    public function reject($id){
        $this->db->update('post', array('disabled'  =>  "1"), array('post_id' =>  $id));
    }
}
?>