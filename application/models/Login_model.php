<?php
class Login_model extends CI_Model {
    
    public function check($data,$check)
	{
        $this->db->select('*');
        $this->db->from('credentials');
        $this->db->where(array('email'  =>  $data['email'],'pass'   =>  md5($data['pass'])));
        $query = $this->db->get();
        $rows = $query->row();
        $result = $query->result_array();
        if($result[0]['blocked'] == "0" || $result[0]['blocked'] == NULL){
            $this->db->update('credentials', array("online" =>  1),array('email'    =>  $data['email']));
            if($rows){
                $sessdata = array(
                    'email'     => $data['email'],
                    'logged_in' => TRUE
                );
                $this->session->set_userdata($sessdata);
                setcookie('email', $data['email'], time()-1);
                if((int)$check == 1)
                    setcookie("email", $data['email'], time()+100000, '/');
                //$_SESSION['email'] = $data['email'];
                return "yes";
            }
            else{
                return "no";
            }
        }
        else if($result[0]['blocked'] == "1"){
            return "blocked";
        }
    }
    public function offline_update(){
        $this->db->update('credentials', array("online" =>  0),array('email'    =>  $this->session->email));
    }
    public function return_current_user_info(){
        $this->db->select('*');
        $this->db->from('credentials');
        $this->db->where('email',$this->session->email);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function users_load(){
        $this->db->select('*');
        $this->db->from('credentials');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function block_unblock_user($id,$type){
        $this->db->update('credentials', array("blocked" => $type), array('c_id' => $id));
    }
}
