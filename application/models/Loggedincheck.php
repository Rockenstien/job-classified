<?php
class Loggedincheck extends CI_Model{

    public function check(){
        if(!$this->session->has_userdata('logged_in') && !isset($_COOKIE['email'])){
            $this->load->view('login');
        }else if(isset($_COOKIE['email'])){
            return "yes";
        }
        else if($this->session->has_userdata('logged_in'))
            return "yes";
        else{
            $this->load->view('login');
        }    
    }
    
}
?>