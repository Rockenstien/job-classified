<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/register/register.js"></script>
</head>
<body>
<?php echo validation_errors('<div class="alert alert-danger">','</div>')?>
    <div class="container">
        <div class="card">
          <div class="card-header text-center">Register</div>
           
            <div class="card-body">
              <?php echo form_open('Register/validate', array('class' =>  'jsform'));?>
              <div class="form-row">
                <div class="col-md-12 mb-3">
                  <label for="fname">First name</label>
                  <input type="text" class="form-control" name="fname" id="fname" placeholder="First name"  required>
                </div>
                <div class="col-md-12 mb-3">
                  <label for="email">Email</label>
                  <div class="input-group">
                    <input type="email" name="email" class="form-control" id="email" placeholder="Email" aria-describedby="inputGroupPrepend2" required>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="col-md-12 mb-3">
                  <label for="validationpass">Password</label>
                  <input type="password" name="pass" class="form-control" id="validationpass" placeholder="Password" required>
                </div>
              <div class="col-md-12 mb-3">
                <label for="validationDefault05">Account Type</label>
                <select class="form-control" name="actype" id="validationDefault05">
                  <option value="ja">Job Advertiser</option>
                  <option value="js">Job seeker</option>
                </select>
              </div>
            </div>
            <input type="hidden" id="base" value="<?php echo base_url(); ?>">
            <!-- <input type="submit" class="btn btn-primary" id="register" value="Submit"> -->
            <button class="btn btn-primary" type="submit"  id="register">Submit form</button>
            <?php echo form_close();?>
          </div>
        </div>
      </div>
</body>
</html>