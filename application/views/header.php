<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/post/post.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<body>
<script>
    var data;
    var online;
    var name;
    function navchat(e){
      //let data = $(this).attr("data-id");
      data = e.getAttribute("data-id");
      online = e.getAttribute("data-online");
      name = e.innerText;
      control = e;
      $("#send").attr("id","send-nav");
      $("#refresh").attr("id","refresh-nav");
            //alert(data);
            
            fetch_messages();
            $("#email").val(data);
            
            
    }
    $(document).on('click','#refresh-nav',function(){
      fetch_messages();
    });
      function fetch_messages(){
              $("#chatlist").empty();
              $.ajax({
                type:"get",
                url:"<?php echo base_url();?>index.php/Dashboard/messages?cont=fnavchat&mail="+data,
                success:function(d){
                    $("#chatlist").append(d);
                    $("#chattitle").empty();
                      if(online == "1")
                        $("#chattitle").html('Chat with '+name+'<span class="badge badge-success">Online</span>');
                      else if(online == "0")
                      $("#chattitle").html('Chat with '+name+'<span class="badge badge-secondary">Offline</span>');
                    //alert(d);
                },
                error: function(){      
                    alert('Error while request..');
                }
            });
            }
    $(document).ready(function(){
      $(document).on('blur',"#send-nav",function(){
            $("#send-nav").attr("id","send");
          });
          $(document).on('click',".close",function(){
            $("#send-nav").attr("id","send");
            $("#refresh-nav").attr("id","refresh");
          });
          $(document).on('click', '#send-nav', function(){ 
            let val = $("#email").val()
            let message = $("#message").val();
           $.ajax({
                    type:'get',
                 url:'<?php echo base_url()?>index.php/Dashboard/send_message?id='+val+'&message='+message+'&control=nav',
                  cache: false, 
                 success:function(d){
                  $("#message").val("");
                    //alert(d)
                    },
                  error: function(){      
                   alert('Error while request..');
                    $("#message").val("");
                   }
            });
            fetch_messages();
          });
      
        $("#navbarDropdown").click(function(){
            function fetch_names(){
                $.ajax({
                    type:'post',
                    url:'<?php echo base_url();?>index.php/Dashboard/response',
                    cache: false,
                    success:function(data){
                        //let jsd = JSON.parse(data);
                      //alert(data);
                        $("#response").empty();
                        $("#response").append(data);
                    }
                });
            } fetch_names();
            setInterval(fetch_names(),1000);
        });
        function fetch_senders(){
                $.ajax({
                    type:'post',
                    url:'<?php echo base_url();?>index.php/Dashboard/messages?cont=fnav',
                    cache: false,
                    success:function(data){
                        //alert(data);
                        $("#messages").empty();
                        $("#messages").append(data);
                        $("#unseen").text($("#msg_count").val());
                    }
                });
                
            } fetch_senders();
            setInterval(fetch_senders(),1000);
        
    });
</script>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="<?php echo base_url();?>">Job Classified</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
    <input type="hidden" id="email">
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    <?php if(empty($this->session->email)){?>
    <li class="nav-item">
        <a class="nav-link active" href="<?php echo base_url()."index.php/Login"?>">Login</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" href="<?php echo base_url()."index.php/Register"?>">Sign Up</a>
      </li>
    <?php }
    if(!empty($this->session->email)){
    if($u_info[0]['user_type'] == "admin"){
  ?>
  <li class="nav-item">
  <a class="nav-link active" href="<?php echo base_url()."index.php/Dashboard/load_post_request";?>">Requests</a>
  </li>
  <li class="nav-item">
  <a class="nav-link active" href="<?php echo base_url()."index.php/Dashboard/load_users";?>">Users</a>
  </li>
    <?php } ?>
    <li class="nav-item">
        <a class="nav-link active" href="<?php echo base_url()."index.php/Logout";?>">Logout</a>
      </li>
  <?php } ?>
    <?php if(!empty($this->session->email)){ 
      if($u_info[0]['user_type'] == "ja"){?>
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url()."index.php/Post";?>">Post <span class="sr-only">(current)</span></a>
      </li>
      <?php 
    }
      }
      if(!empty($u_info)){
      if($u_info[0]['user_type'] == "ja"){?>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Responses
        </a>
        <div class="dropdown-menu" id="response" aria-labelledby="navbarDropdown"></div>
      </li>
      <?php }}
      if(!empty($this->session->email)){ ?>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownmsg" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Messages <span class="badge badge-light" id="unseen"></span>
        <span class="sr-only">unread messages</span>
        </a>
        <div class="dropdown-menu" id="messages" aria-labelledby="navbarDropdown"></div>
      </li>
      
      <?php
      }
       if(!empty($u_info)){ ?>
        <span class="navbar-text" style="color:white;">
            Welcome, <?php echo $u_info[0]['f_name']?>
        </span>
      <?php } ?>
      
    </ul>
</div>
</nav>