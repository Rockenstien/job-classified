<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <title>Document</title>
    <script>
        $(document).ready(function(){
            var log = $("#loggedin").val();
            if(log == "1"){
                setInterval(function(){
                    $(location).attr('href', '<?php echo base_url()?>');}
                    ,1000);
            }
        });
    </script>
</head>
<body>
<div class="container">
<?php echo validation_errors('<div class="alert alert-danger">','</div>')?>
        <div class="card">
            <div class="card-header text-center">
                Login
            </div>
            <?php 
            
            if(isset($auth)){if($auth == "yes"){ ?> <!-- php block -->
                <div class="alert alert-success" role="alert">
                    Logging you in!
                </div>
                <?php if(!empty($this->session->email)){ ?>

                
                <input type="hidden" id="loggedin" value="1">
                <?php }?>
                <?php } else if($auth == "blocked"){?>
                    <div class="alert alert-danger" role="alert">
                        Your account is blocked!! Contact to the admin
                    </div>
                <?php } else if($auth == "no"){?>
                <div class="alert alert-danger" role="alert">
                    Wrong email or password!
                </div>
                <input type="hidden" id="loggedin" value="0">
            <?php }} ?>
            <input type="hidden" id="loggedin" value="0">
            <div class="card-body">
            <?php echo form_open('Login/authenticate',array('class'=>'loginform'));?>
            <div class="form-group">
                <label for="email">Email address</label>
                <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="pass">Password</label>
                <input type="password" name="pass" class="form-control" id="pass" placeholder="Password">
            </div>
            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="check">
                <label class="form-check-label" name="check" for="check">Remember Password?</label>
            </div>
            <input type="hidden" id="base" value="<?php echo base_url(); ?>">
            <button type="submit" class="btn btn-primary ">Login!</button>
            <div id="loading" class="text-primary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <div class="alert alert-warning fade show" role="alert">
            <strong>Holy guacamole!</strong> You should create an account if you don't have one, from <a class="alert-link" href="<?php echo base_url(); ?>index.php/Register">here.</a>
        </div>
        <? echo form_close();?>
        <div class="card-footer text-muted">
            Created by Paras Kumar &copy; 
        </div>
    </div>
</body>
</html>