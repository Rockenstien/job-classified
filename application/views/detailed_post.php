<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/post/post.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script>
      $(document).ready(function(){
        $("#chatbutton").click(function(){
        let pid = $("#pid").val();
        setInterval(function() {
          $.ajax({
                type:'get',
                url:'<?php echo base_url()?>index.php/Dashboard/messages?id='+pid,
                cache: false, 
                success:function(d){
                  $("#chatlist").empty();
                  $("#chatlist").html(d);
                  //alert(d);
                },
                error: function(){      
                  alert('Error while request..');
                }
                });}, 1000);
        $("#send").click(function(){
          let message = $("#message").val();
          $.ajax({
              type:'get',
              url:'<?php echo base_url()?>index.php/Dashboard/send_message?id='+pid+'&message='+message,
              cache: false,
              success:function(){
                $("#message").val("");
              }, 
              error: function(){      
                alert('Error while request1..');
              }
          });
          });
        });
      });
    </script>
    <style>
    ul{
      padding-left:0px;
    }
    .chat-list{
      width:100%;
    }
      .from{
        text-align:left;
        
      }
      .you{
        text-align:right;
        
      }
    </style>
    <title>Document</title>
</head>
<body>

<div class="jumbotron">
  <h1 class="display-4"><?php echo $rows[0]['title'];?></h1>
  <p class="lead"><?php echo $rows[0]['description']?></p>
  <hr class="my-4">
  <p>Updated <?php $dd = date_diff(new DateTime(date("Y-m-d")),new DateTime($rows[0]['date'])); echo $dd->format("%d days");?> ago</p>
  <!-- <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a> -->
  <?php if($rows[0]['email'] != $this->session->email) {?>
    <button type="button" id="chatbutton" class="btn btn-success" data-toggle="modal" data-target="#chat">
      Chat
    </button>
</div>
<input type="hidden" id=pid value=<?php echo $id;?>>
<!-- Modal -->
<div class="modal fade" id="chat" tabindex="-1" role="dialog" aria-labelledby="#chattitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="chattitle">Chat with <?php echo $rows[0]['f_name'];?><span class="badge badge-<?php if($rows[0]['online'] == "1") echo 'success">Online'; else { echo 'secondary">Offline';}?></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card" style="width: 100%;">
          <ul class="chat-list" id="chatlist"></ul>
        </div>
        
      </div>
      <div class="modal-footer">
      <textarea style="width: 100%; height:100px;resize:none;" id="message" cols="30" rows="10" required></textarea>
          <button type="button" id="send" class="btn btn-success">Send</button>
      </div>
    </div>
  </div>
</div>
<!-- end chat -->
<?php } ?>


</body>
</html>