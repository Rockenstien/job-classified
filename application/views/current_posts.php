<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/post/post.js"></script>
</head>
<body>

  <?php 
    for($i=0;$i<sizeof($rows);$i++){
  ?>
    <div class="card mb-3" style="max-width: 540px;" data-row_id="<?php echo $rows[$i]; ?>">
  <div class="row no-gutters">
    <div class="col-md-4">
      <img src="<?php echo $rows[$i]['imgfile']?>" class="card-img" alt="<?php echo explode("/",$rows[$i]['imgfile'])[2]?>">
    </div>
    <div class="col-md-8">
      <div class="card-body">
        <h5 class="card-title"><?php echo $rows[$i]['title']?></h5>
        <p class="card-text"><?php echo $rows[$i]['description']?></p>
        <p class="card-text"><small class="text-muted">Last updated <?php $dd = date_diff(date("Y-m-d"),$rows[$i]['date']); echo $dd;?> days ago</small></p>
        <p class="card-text"><small class="text-muted">Sender - <span><?php echo $rows[$i]['email']?></span></small></p>
        <button class="btn btn-primary">Contact</button>
      </div>
    </div>
  </div>
</div>
  <?php
    }
  ?>



<?php echo validation_errors();?>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
      <?php echo form_open_multipart('Post/upload_post');?>
      <div class="card">
       <div class="card-header text-center">Edit Post</div>
         <div class="card-body">
   <div class="form-group row">
    <label for="adtitle" class="col-sm-2 col-form-label">Title</label>
    <div class="col-sm-10">
      <input type="text" name="adtitle" class="form-control" id="adtitle" placeholder="Title">
    </div>
  </div>
  <div class="form-group row">
    <label for="description" class="col-sm-2 col-form-label" style="vertical-align:top">Description</label>
    <div class="col-sm-10">
        <textarea name="description" class="form-control" placeholder="Description" id="description" cols="30" rows="10" style="vertical-align:top;resize:none;"></textarea>
    </div>
  </div>
  <div class="form-group row">
    <label for="image" class="col-sm-2 col-form-label" >Advertisement Image</label>
    <div class="col-sm-10">
        <input class="form-control-file" type="file" name="imgfile" id="imgfile">
    </div>
  </div>
  <input type="hidden" id="base" value="<?php echo base_url(); ?>">
  <div class="form-group row">
    <div class="col-sm-10">
      <button class="btn btn-primary">Post</button>
    </div>
  </div>    
    </div>
</div>
<?php echo form_close();?>



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>





</body>
</html>