<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <title>Document</title>
    <script>
        $(document).ready(function(){
            $(".approved").hide();
            $("#mailsentcheck").hide();
            let len = $(".card").length;
            
            $(function(){
                for(let i=1; i<=len; i++){
                    let data = $("#card" +i).attr("data-id");

                    $(document).on('click',"#detail" +i,function(){
                      let title = $('#title' +i).text();
                      let des = $('#desf' +i).val();
                      let imgsrc = $('#img' +i).attr("src");
                      let imgalt = $('#img' +i).attr("alt");
                      $('#detail-body').text(des);
                      $('#detail-title').text(title);
                      $('#detail-img').attr("src",imgsrc);
                      $('#detail-img').attr("alt",imgalt);
                    });
                    
                    $(document).on('click','#approve'+i,function(){
                        alert(data);
                        if(confirm("Are you sure?")){
                            
                            $.ajax({
                                url: "<?php echo base_url();?>index.php/Post_request/approve?id="+data,
                                method : "get",
                                success:function(){
                                    $("#approved" +i).show(1000);
                                    
                                }
                            });
                        }
                    });
                    $(document).on('click','#reject'+i,function(){
                      let recipient = $(this).data('whatever');
                      $("#recipient-name").val(recipient);
                      $(document).on('click','#send-message',function(){
                        let message = $("#message-text").val();
                        if(confirm("Are you sure")){
                          $.ajax({
                          method: "get",
                          url: "<?php echo base_url();?>index.php/Post_request/reject_mail?rec="+recipient+"&message="+message+"&id="+data,
                          success:function(de){
                            
                            if(de == "1"){
                              $("#mailsentcheck").addClass("alert-success");
                              $("#mailsentcheck").append("Mail Sent");
                              $("#mailsentcheck").show();
                            }
                            else if(de == "0"){
                              $("#mailsentcheck").addClass("alert-danger");
                              $("#mailsentcheck").append("Mail Not Sent");
                              $("#mailsentcheck").show();
                            }
                          }
                        });
                        }
                      });
                    });
                    
                   
                }
            })
        });
    
    </script>
</head>
<body>
<?php 
    $counts = 1;
    if(empty($rows)){
 ?>
 <div class="alert alert-dark" role="alert">
  There are no pending post requests
</div>
 <?php       
    }
    foreach($rows as $ads){ 
        if($ads['disabled'] == "0" && $ads['approved'] == "0"){
  ?>
<div class="card mb-3" id="card<?php echo $counts;?>" style="max-width: 540px;" data-id="<?php echo $ads['post_id']; ?>">
        <!-- <input type="hidden" id="data<?php //echo $counts?>" name="data" value="<?php echo $ads['post_id']; ?>"> -->
  <div class="row no-gutters">
    <div class="col-md-4">
      <img id="img<?php echo $counts;?>" src="<?php echo $ads['imgfile']?>" class="card-img" alt="">
    </div>
    <div class="col-md-8">
      <div class="card-body">
        <h5 class="card-title" id="title<?php echo $counts; ?>"><?php echo $ads['title']?></h5>
        <p class="card-text" id="des<?php echo $counts; ?>"><?php echo substr($ads['description'],0,200); ?>...</p>
        <input type="hidden" id="desf<?php echo $counts; ?>" value ="<?php echo htmlspecialchars($ads['description']);?>">
        <p>.....more</p>
        <p class="card-text"><small class="text-muted">Last updated <?php $dd = date_diff(new DateTime(date("Y-m-d")),new DateTime($ads['date'])); echo $dd->format("%d");?> days ago</small></p>
        <p class="card-text"><small class="text-muted">Sender - <span><?php echo $ads['f_name']?></span></small></p>
       
        <button type="button" id="approve<?php echo $counts;?>" class="btn btn-success">
             Approve
        </button>
        <button type="button" id="reject<?php echo $counts;?>" data-toggle="modal" data-target="#mail-modal" data-whatever="<?php echo $ads['email'];?>" class="btn btn-danger">
            Reject    
        </button>
        <button type="button" id="detail<?php echo $counts;?>" class="btn btn-primary" data-toggle="modal" data-target="#details">
            Details
        </button>   
            
        </div>
    </div>
  </div>
  <div class="alert alert-success approved" id="approved<?php echo $counts;?>" role="alert">
            Approved
</div>
</div>
<?php 
    }
}    
?>
<!-- modal for details -->

<div class="modal fade" id="details" tabindex="-1" role="dialog" aria-labelledby="details-title" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="detail-title"></h5>
        
        <div class=" col col-md-4">
      <img src="" id="detail-img" class="card-img" alt="">
    </div>
      </div>
      <div class="modal-body" id="detail-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<!-- end of modal for detials -->
<!-- modal for email -->
<div class="modal fade" id="mail-modal" tabindex="-1" role="dialog" aria-labelledby="mailmodallabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="mailmodallabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Recipient:</label>
            <input type="text" class="form-control" id="recipient-name" readonly>
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control" id="message-text"></textarea>
          </div>
        </form>
        <div class="alert" id="mailsentcheck" role="alert"></div>
      </div>
      <div class="modal-footer">
      
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="send-message" class="btn btn-primary">Send message</button>
      </div>
    </div>
  </div>
</div>
<!-- modal for email -->
</body>
</html>