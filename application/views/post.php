<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/post/post.js"></script>
</head>
<body>
<?php echo validation_errors();?>
<?php echo form_open_multipart('Post/upload_post');?>
<div class="card">
    <div class="card-header text-center">Add Post</div>
   <div class="card-body">
   <div class="form-group row">
    <label for="adtitle" class="col-sm-2 col-form-label">Title</label>
    <div class="col-sm-10">
      <input type="text" name="adtitle" class="form-control" id="adtitle" placeholder="Title">
    </div>
  </div>
  <div class="form-group row">
    <label for="description" class="col-sm-2 col-form-label" style="vertical-align:top">Description</label>
    <div class="col-sm-10">
        <textarea name="description" class="form-control" placeholder="Description" id="description" cols="30" rows="10" style="vertical-align:top;resize:none;"></textarea>
    </div>
  </div>
  <div class="form-group row">
    <label for="image" class="col-sm-2 col-form-label" >Advertisement Image</label>
    <div class="col-sm-10">
        <input class="form-control-file" type="file" name="imgfile" id="imgfile">
    </div>
  </div>
  <input type="hidden" id="base" value="<?php echo base_url(); ?>">
  <div class="form-group row">
    <div class="col-sm-10">
      <button class="btn btn-primary">Post</button>
    </div>
  </div>    
    </div>
</div>
<?php echo form_close();?>
</body>
</html>