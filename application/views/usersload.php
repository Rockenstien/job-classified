<script>
    $(document).ready(function(){
        
        let rowCount = $('#mytable tr').length;
        
        $(function(){
            for(let i=1;i<=rowCount;i++){
                let block = $("#block" +i).text();
                if(block == "No"){
                    $("#blck" +i).addClass("btn-danger");
                    $("#blck" +i).text("Block");
                }
                else if(block == "Yes"){
                    $("#blck" +i).addClass("btn-success");
                    $("#blck" +i).text("Unblock");
                    $("#blck" +i).attr("id","unblck" +i);
                }
                $(document).on('click','#blck'+i,function(){
                    let id = $("#id"+i).val();
                    $.ajax({
                        method:"get",
                        url:"<?php echo base_url();?>index.php/Dashboard/blockuser?id="+id,
                        success:function(data){
                            $("#blck" +i).removeClass("btn-danger");
                            $("#blck" +i).addClass("btn-success");
                            $("#blck" +i).text("Unblock");
                            $("#blck" +i).attr("id","unblck" +i);
                        }
                    });
                });
                $(document).on('click','#unblck'+i,function(){
                    let id = $("#id"+i).val();
                    $.ajax({
                        method:"get",
                        url:"<?php echo base_url();?>index.php/Dashboard/unblockuser?id="+id,
                        success:function(data){
                            $("#unblck" +i).removeClass("btn-success");
                            $("#unblck" +i).addClass("btn-danger");
                            $("#unblck" +i).text("Block");
                            $("#unblck" +i).attr("id","blck" +i);
                        }
                    });
                });
            }
        });
    });
</script>
<table id="mytable" class="table table-hover">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Username</th>
      <th scope="col">Email</th>
      <th scope="col">User Type</th>
      <th scope="col">Professional Account Status</th>
      <th scope="col">Blocked Account Status</th>
      <th scope="col">Block</th>
      
    </tr>
  </thead>
  <tbody>
<?php if(isset($rows)){
    $counts = 1;
    foreach($rows as $users){
        
    ?>
    <tr>
      <th scope="row"><?php echo $counts;?></th>
      <input type="hidden" id="id<?php echo $counts; ?>" value="<?php echo $users['c_id']?>" >
      <td><?php echo $users['f_name']?></td>
      <td><?php echo $users['email']?></td>
      <td><?php switch($users['user_type']){ case "admin" : echo "Admin";break; case "ja" : echo "Advertiser";break; case "js" : echo "Job Seekers";break;}?></td>
      <td><?php switch($users['pro']){ case "1" : echo "Yes";break; case "0" : echo "No";break;}?></td>
      <td id="block<?php echo $counts;?>"><?php switch($users['blocked']){ case "1" : echo "Yes";break; case "0" : echo "No";break; default : echo "No";break;}?></td>
      <td><button class="btn" id="blck<?php echo $counts;?>"></button></td>
    </tr>
<?php $counts++;}}?>
</tbody>
</table>