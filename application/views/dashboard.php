<script>
        function chatnav(iddata){
                        $("#chat-nav").click(function(){
                          let message = $("#message").val();
                        $.ajax({
                            type:'get',
                            url:'<?php echo base_url()?>index.php/Dashboard/send_message?id='+iddata+'&message='+message+'control=nav',
                            cache: false, 
                            success:function(d){
                              $("#message").val("");
                              //alert(d)
                            },
                            error: function(){      
                              alert('Error while request..');
                              $("#message").val("");
                            }
                          });
                      });
                        }
        $(document).ready(function(){
            let len = $(".card").length;
            
            $(function(){
              
                for(let i=1; i<=len; i++){
                  var fname ;
                  var online ;
                  let data = $("#card" +i).attr("data-id");
                  
                    $.ajax({
                      type:'get',
                        url:'<?php echo base_url();?>index.php/Dashboard/apply?control=check',
                        cache:false,
                        success:function(d){
                          let jsd = JSON.parse(d);
                          $.each( jsd, function( key, val ) {
                            $("[data-buid="+val.post_id+"]").attr("disabled",true);
                            $("[data-buid="+val.post_id+"]").text("Applied");
                           });

                        }
                    });
                    
                    $("#apply" +i).click(function(){
                      
                      $.ajax({
                        type:'get',
                        url:'<?php echo base_url();?>index.php/Dashboard/apply?id='+data+'&control=apply',
                        cache:false,
                        success:function(){
                          $("#apply" +i).attr("disabled",true);
                          $("#apply" +i).text("Applied");

                        }
                      });
                    });
                    $("#edit" +i).click(function(){
                      //alert(data);
                       fdata =  $("#pd").val(data);
                       let title =  $("#adtitlem").val($("#title" +i).text());
                      let des =  $("#descriptionm").val($("#des" +i).text());
                       
                        //console.log(data); //complete by ajax
                    });
                    $("#delete" +i).click(function(){
                        if(confirm("Are you sure?")){
                            $(location).attr('href','<?php echo base_url();?>index.php/Dashboard/delete_post?id='+data);
                        }
                    });
                    
                    $(document).on('click',"#detail-button" +i,function(){
                      let title = $('#title' +i).text();
                      let des = $('#desf' +i).val();
                      let imgsrc = $('#img' +i).attr("src");
                      let imgalt = $('#img' +i).attr("alt");
                      $('#detail-body').text(des);
                      $('#detail-title').text(title);
                      $('#detail-img').attr("src",imgsrc);
                      $('#detail-img').attr("alt",imgalt);
                    });
                    $(document).on('click','#refresh',function(){
                      msg_check();
                    });
                    $("#chatbutton" +i).click(function(){
                      fname = $("#fname" +i).val();
                      online = $("#online" +i).val();
                      data = $("#card" +i).attr("data-id");
                      //$("#chatlist").empty();
                      
                      msg_check();
                      
                    $(".close").click(function(){
                      clearInterval(msg_check);
                    });
                      $("#send").click(function(){
                        let message = $("#message").val();
                        $.ajax({
                            type:'get',
                            url:'<?php echo base_url()?>index.php/Dashboard/send_message?id='+data+'&message='+message+'&control=mainpage',
                            cache: false, 
                            success:function(d){
                              $("#message").val("");
                              //alert(d)
                            },
                            error: function(){      
                              alert('Error while request..');
                              $("#message").val("");
                            }
                          });
                      });
                    });
                    function msg_check() {
                        $("#chattitle").empty();
                        //alert(data);
                      if(online == "1")
                        $("#chattitle").html('Chat with '+fname+'<span class="badge badge-success">Online</span>');
                      else if(online == "0")
                      $("#chattitle").html('Chat with '+fname+'<span class="badge badge-secondary">Offline</span>');
                        $.ajax({
                            type:'get',
                            url:'<?php echo base_url()?>index.php/Dashboard/messages?id='+data+'&cont=fpost',
                            cache: false, 
                            success:function(d){
                              $("#chatlist").html(d);
                             },
                            error: function(){      
                              alert('Error while request..');
                            }
                          });
                    }
                   
                }
            });
        });
    </script>
    <style>
    ul{
      padding-left:0px;
    }
    .chat-list{
      width:100%;
    }
      .from{
        text-align:left;
        
      }
      .you{
        text-align:right;
        
      }
    </style>
</head>
<body>
  <?php
    if(empty($this->session->email)){
      echo '<div class="alert alert-warning">Please <a class="alert-link" href="'.base_url().'index.php/Login">Login</a> to apply for the jobs</div>';
    }
  $counts = 1;
    foreach($rows as $ads){
       if($ads['approved'] == 1 or $this->session->email == $ads['email']){ 
  ?>
   <div class="container">
    <div class="row">
    <div class="card mb-3" id="card<?php echo $counts;?>" style="max-width: 540px;" data-id="<?php echo $ads['post_id']; ?>">
        
  <div class="row no-gutters">
    <div class=" col col-md-3">
      <img src="<?php echo $ads['imgfile']?>" id="img<?php echo $counts;?>" class="card-img" alt="<?php echo $ads['title']?>">
    </div>
    <div class="col col-md-8" >
      <div class="card-body">
        <h5 class="card-title" id="title<?php echo $counts; ?>"><?php echo $ads['title']?></h5>
        <p class="card-text" id="des<?php echo $counts; ?>"><?php echo substr($ads['description'],0,200); ?>...</p>
        <input type="hidden" id="desf<?php echo $counts; ?>" value ="<?php echo htmlspecialchars($ads['description']);?>">
        <p>.....more</p>
        <p class="card-text"><small class="text-muted">Last updated <?php $dd = date_diff(new DateTime(date("Y-m-d")),new DateTime($ads['date'])); echo $dd->format("%d");?> days ago</small></p>
        <p  class="card-text"><small class="text-muted">
        <?php
        if($this->session->email == $ads['email']){
        if($ads['approved'] == "1"){
         
          echo '<span class="alert alert-success">You post is Approved</span>';
        }
        else if($ads['disabled'] == "1"){
         
          echo '<span class="alert alert-danger">You post is Rejected</span>';
        }
        else if($ads['approved'] == "0" && $ads['disabled'] == "0"){
          echo '<span class="alert alert-primary">Admin Approval Pending</span>';
        }
      }
        ?></small></p>
        <p class="card-text"><small class="text-muted">Sender - <span><?php echo $ads['f_name']?></span></small></p>
        <?php if(!empty($this->session->email)&& $ads['email'] != $this->session->email){?>
          <?php if(!empty($u_info)){
        if($u_info[0]['user_type'] == "js"){?>
        <button type="button" id="chatbutton<?php echo $counts;?>" class="btn btn-success" data-toggle="modal" data-target="#chat">
          Chat
        </button>
        <?php }}} ?>
        <?php if($ads['email'] == $this->session->email){?>
        <button type="button" id="edit<?php echo $counts;?>" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
             Edit
        </button>
        <button type="button" id="delete<?php echo $counts;?>" class="btn btn-primary">
            Delete    
        </button>
        <?php } ?>
        <button type="button" class="btn btn-primary" id="detail-button<?php echo $counts;?>" data-toggle="modal" data-target="#details">
          Details
        </button>
        <?php 
        if(!empty($u_info)){
        if($u_info[0]['user_type'] == "js"){?>
        <button type="button" class="btn btn-dark" id="apply<?php echo $counts;?>" data-buid="<?php echo $ads['post_id'];1?>">Apply</button>
        <?php }} ?>
        </div>
    </div>
  </div>
</div>
</div>
</div>
<!-- for chat -->
    <!-- Button trigger modal -->

  <?php
   $counts++;
       }
    }
  ?>
<!-- for edit -->
<?php echo validation_errors('<div class>');?>
<?php echo form_open_multipart('Dashboard/edit_post');?>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <input type="hidden" name="pd" id="pd">
      <div class="modal-body">
      

        <div class="card">
            <div class="card-header text-center">Edit Post</div>
        <div class="card-body">
        <div class="form-group row">
            <label for="adtitlem" class="col-sm-2 col-form-label">Title</label>
            <div class="col-sm-10">
            <input type="text" name="adtitlem" class="form-control" id="adtitlem" placeholder="Title">
            </div>
        </div>
        <div class="form-group row">
            <label for="descriptionm" class="col-sm-2 col-form-label" style="vertical-align:top">Description</label>
            <div class="col-sm-10">
                <textarea name="descriptionm" class="form-control" placeholder="Description" id="descriptionm" cols="30" rows="10" style="vertical-align:top;resize:none;"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="imgfilem" class="col-sm-2 col-form-label" >Advertisement Image</label>
            <div class="col-sm-10">
                <input class="form-control-file" type="file" name="imgfilem" id="imgfilem">
            </div>
        </div>
       
           
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div> 
            </div>
        </div>
        
       

      </div>
      
    </div>
  </div>
</div>
<?php echo form_close();?>
<!-- end of edit -->
<!-- for chat -->
    <!-- Button trigger modal -->

    <?php 
    $counts = 1;
    foreach($rows as $ads){
       if($ads['approved'] == 1){ 
        if($ads['email'] != $this->session->email){
  ?>
  <input type="hidden" id="fname<?php echo $counts;?>" name="" value ="<?php echo $ads['f_name'];?>">
  <input type="hidden" id="online<?php echo $counts;?>" name="" value="<?php echo $ads['online'];?>">

  
  <?php
        }
   $counts++;
       
    }
    }  
  ?>
<!-- Modal for nav-->
<div class="modal fade" id="chat" tabindex="-1" role="dialog" aria-labelledby="#chattitle" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="chattitle"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card" style="width: 100%;">
          <ul class="list-group" id="chatlist"></ul>
        </div>
        
      </div>
      <div class="modal-footer">
      <textarea style="width: 100%; height:100px;resize:none;" id="message" cols="30" rows="10" required></textarea>
          <button type="button" id="send" class="btn btn-success">Send</button>
          <button type="button" id="refresh" class="btn btn-success">Refresh</button>
      </div>
    </div>
  </div>
</div>
<!-- end chat -->

<!-- modal for details -->

<div class="modal fade" id="details" tabindex="-1" role="dialog" aria-labelledby="details-title" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="detail-title"></h5>
        
        <div class=" col col-md-4">
      <img src="" id="detail-img" class="card-img" alt="">
    </div>
      </div>
      <div class="modal-body" id="detail-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<!-- end of modal for detials -->
</body>
</html>