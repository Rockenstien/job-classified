<link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
<div class="card mb-3" style="max-width: 540px;" data-row_id="<?php echo $id; ?>">
  <div class="row no-gutters">
    <div class="col-md-4">
      <img src="..." class="card-img" alt="...">
    </div>
    <div class="col-md-8">
      <div class="card-body">
        <h5 class="card-title">Advertisement Title</h5>
        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
        <p class="card-text"><small class="text-muted">Sender - <span>ABC Company</span></small></p>
        <button class="btn btn-primary">Contact</button>
      </div>
    </div>
  </div>
</div>