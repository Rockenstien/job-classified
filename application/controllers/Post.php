<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('Post_model');
    }
	public function index(){
        if($this->lc->check()){
            $this->load->view('post');
        }
    }
    public function uploader(){
        $this->form_validation->set_rules('adtitle','Title','required');
        $this->form_validation->set_rules('description','Description','required');
        if (empty($_FILES['imgfile']['name'])){
            $this->form_validation->set_rules('imgfile', 'Document', 'required');
        }
        if(isset($_FILES["imgfile"]["name"])) {
            $config['upload_path'] = 'uploads/adimage/';
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_size']     = '100';
            $config['max_width'] = '1024';
            $config['max_height'] = '768';
            $this->upload->initialize($config);
            $this->upload->overwrite = true;
            if ( !$this->upload->do_upload('imgfile')){
                    echo $this->upload->display_errors();  
                }
            else{
                $data = $this->upload->data();
                $img_url = base_url().'uploads/adimage/'.$data['file_name'];
                $rows = $this->Post_model->givecid();
                $form = array(
                    'title' =>  $this->input->post('adtitle'),
                    'description'   =>  $this->input->post('description'),
                    'date'  =>  date("Y-m-d"),
                    'c_id' =>  $rows[0]['c_id'],
                    'imgfile'   =>  $img_url,
                    'disabled'  => 0,
                    'approved'  =>  0
                );
                $rows1 = $this->Post_model->postupload($form);
                redirect(base_url());
                //echo "<pre>"; //check
                //var_dump($rows);
                //$data = $this->upload->data();  
                //echo '<img src="'.base_url().'upload/'.$data["file_name"].'" width="300" height="225" class="img-thumbnail" />'; 
            }
        }
    }
    
    public function upload_post(){ 
        $cid_posts = [];
        $cid = $this->Post_model->givecid();
        $full_info = $this->Post_model->get_credentials($cid[0]['c_id']);
        $posts = $this->Post_model->get_post_upload_post($cid[0]["c_id"]);
        echo "<pre>";
        
        foreach($posts as $pro){
            if($pro['c_id'] == $cid[0]['c_id']){
                array_push($cid_posts,$pro);
                
            }
        }
        
        
        if($full_info[0]['pro'] == "1"){
            $this->uploader();
        }
        else if(array_key_last($cid_posts) <= 4){
            $this->uploader();
            
        }
        //echo count($cid_posts[0]);
        
    }
    
}
