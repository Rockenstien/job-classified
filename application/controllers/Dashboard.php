<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct(){
		parent::__construct();
		$this->load->model('Post_model');
		$this->load->model('Login_model');
    }
    public function index()
	{
		//$this->load->view('dashboard',$data);
		
			$rows = $this->Post_model->dashposts(); //dashboard posts
			$current_user_info = $this->Login_model->return_current_user_info();
			$data['rows'] = $rows;
			$data['u_info'] = $current_user_info;
			$this->load->view('header',$data);
			$this->load->view('dashboard',$data);
		
		//echo "<pre>";
		//var_dump($rows);
	}
	public function edit_post(){
        $this->form_validation->set_rules('adtitlem','Title','required');
        $this->form_validation->set_rules('descriptionm','Description','required');
        if (empty($_FILES['imgfilem']['name'])){
            $this->form_validation->set_rules('imgfilem', 'Document', 'required');
        }
        if(isset($_FILES["imgfilem"]["name"])) {
            $config['upload_path'] = 'uploads/adimage/';
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_size']     = '100';
            $config['max_width'] = '1024';
            $config['max_height'] = '768';
            $this->upload->initialize($config);
            $this->upload->overwrite = true;
            if ( !$this->upload->do_upload('imgfilem')){
                    echo $this->upload->display_errors();  
                }
            else{
                $data = $this->upload->data();
                $img_url = base_url().'uploads/adimage/'.$data['file_name'];
                $rows = $this->Post_model->givecid();
                $form = array(
                    'title' =>  $this->input->post('adtitlem'),
                    'description'   =>  $this->input->post('descriptionm'),
                    'date'  =>  date("Y-m-d"),
                    'c_id' =>  $rows[0]['c_id'],
					'imgfile'   =>  $img_url,
					'approved'  =>  0,
					'disabled'	=> 0
                );
                $this->Post_model->postedit($form,$this->input->post('pd'));
                $this->index();
                //$data = $this->upload->data();  
                //echo '<img src="'.base_url().'upload/'.$data["file_name"].'" width="300" height="225" class="img-thumbnail" />'; 
            }
        }
	}
	public function delete_post(){
		$id = $this->input->get("id");
		//var_dump($id);
		$this->Post_model->deletepost($id);
		$this->index();
	}
	public function details(){
		$id = $this->input->get('id');
		$rows = $this->Post_model->detailpost($id); //add changes for different users
		$data['rows'] = $rows;
		$data['id'] = $id;
		//echo "<pre>";
		//var_dump($rows);
		$this->load->view('detailed_post',$data);
		
	}
	public function messages(){
		$msg = "";
		$unseen_counter = 0;
		$empty = 0;
		if(!empty($this->input->get("id"))){
			$id = $this->input->get("id");
		}
		if(!empty($this->input->get("mail"))){
			$mail = $this->input->get("mail");
		}
		$control = $this->input->get("cont");
		$cid = $this->Post_model->givecid();
		
		
		switch($control){
			case "fpost" : {
				$msg = "";
				$rows_cid = $this->Post_model->chat_cid_handler($id);
				$full_message_info = $this->Post_model->getmessage_from_post($rows_cid);
				
				foreach($full_message_info as $msg_info){
					//echo "<pre>";
					//var_dump($rows_cid);
					//var_dump($msg_info['from_user_cid']);
					//var_dump($msg_info['to_user_cid']);
					if($cid[0]['c_id'] == $msg_info['from_user_cid']){
						$this->Post_model->seen_change($msg_info['chat_id']);
						$msg .= '<li class="list-group-item from">'.$msg_info['message'].'<small style="float:right;"class="text-muted text-right">Sent on '.$msg_info['datetime'].'</small></li>';
					}
					else if($cid[0]['c_id'] == $msg_info['to_user_cid']){
						$msg .= '<li class="list-group-item you">'.$msg_info['message'].'<small style="float:left;"class="text-muted text-left">Sent on '.$msg_info['datetime'].'</small></li>';
					}
				}
				if(empty($full_message_info)){
					$msg.= '<li class="list-group-item">No chat history</li>';
				}
				echo $msg;
				break;
			}
			case "fnav" : {
				$msg_sender_name_list = [];
				$unseen_counter = 0;
				$i = 0;
				$full_list = [];
				$unique_name_rec =[];
				$full_message_info = $this->Post_model->getmessage_from_nav($cid[0]['c_id']);
				//var_dump($full_message_info);
				if(!empty($full_message_info)){
					foreach($full_message_info as $msg_info){
						//echo "<pre>";
						//var_dump($rows_cid);
						//var_dump($msg_info['from_user_cid']);
						//var_dump($msg_info['to_user_cid']);
						if($cid[0]['c_id'] == $msg_info['from_user_cid']){
							$name_rec = $this->Post_model->get_credentials($msg_info['to_user_cid']);
							//array_push($msg_sender_name_list,$name_rec[0]['f_name']);
							$msg_sender_name_list[$i]['online'] = $name_rec[0]['online'];
							$msg_sender_name_list[$i][$name_rec[0]['email']] = $name_rec[0]['f_name'];
							
							//var_dump($msg_info);
							if($msg_info['seen'] == "0"){
								$unseen_counter++;
								//echo $unseen_counter;
							}
						}
						$i++;
					}
					foreach($msg_sender_name_list as $data11){
	
	
						array_push($full_list,$data11);
					}
					
						
						//var_dump();
					array_push($unique_name_rec, array_unique($full_list,SORT_REGULAR));
					
					foreach($unique_name_rec as $namerecords){
						//var_dump($val);
						foreach($namerecords as $namerecords1){
							foreach($namerecords1 as $key => $val){
								if($key == "online"){ $online = $val;}
								else $msg.= '<li class="dropdown-item navchatting" data-online="'.$online.'"  onclick="navchat(this)" data-toggle="modal" href="#chat"  data-id="'.$key.'"><i>'.$val.'</i></li>';
							}
						
					}
					}
					$msg .= '<input type="hidden" value="'.$unseen_counter.'" id="msg_count">';
					echo $msg;
				}
				else if(empty($full_message_info)){
					$msg .= '<li class="dropdown-item">No messages</li>';
					$msg .= '<input type="hidden" value="'.$unseen_counter.'" id="msg_count">';
					echo $msg;
				}
				break;
			}
			case "fnavchat" : {
				$msg = "";
				$to_cid = $this->Post_model->get_cid_from_mail($mail);
				$full_message_info = $this->Post_model->open_chat_for_nav($to_cid[0]['c_id']);
				foreach($full_message_info as $msg_info){
					//echo "<pre>";
					//var_dump($msg_info);
					//var_dump($msg_info['from_user_cid']);
					//var_dump($msg_info['to_user_cid']);
					if($cid[0]['c_id'] == $msg_info['from_user_cid']){
						$this->Post_model->seen_change($msg_info['chat_id']);
						$msg .= '<li class="list-group-item from">'.$msg_info['message'].'<small style="float:right;"class="text-muted text-right">Sent on '.$msg_info['datetime'].'</small></li>';
					}
					else if($cid[0]['c_id'] == $msg_info['to_user_cid']){
						$msg .= '<li class="list-group-item you">'.$msg_info['message'].'<small style="float:left;"class="text-muted text-left">Sent on '.$msg_info['datetime'].'</small></li>';
					}
				}
				if(empty($full_message_info)){
					$msg.= '<li class="list-group-item">No chat history</li>';
				}
				echo $msg;
				break;
			}
		}
		
		
		
		
	}
	public function send_message(){
		$id = $this->input->get("id");
		$control = $this->input->get("control");
		$my_cid = $this->Post_model->givecid();
		if(empty($this->input->get("message"))){
			$this->index();
		}
		$message = $this->input->get("message");
		switch($control){
			case "mainpage" : {
				$rows_cid = $this->Post_model->chat_cid_handler($id);
			}break;
			case "nav"	: {
				$cid = $this->Post_model->get_cid_from_mail($id); 
				var_dump($cid);
				$rows_cid['from_cid']['c_id'] = $cid[0]['c_id'];
				$rows_cid['your_cid']['c_id'] = $my_cid[0]['c_id'];
			}break;
		}
		
		//$rows_cid['message'] = $message;
		$timestamp = date("Y-m-d H:i:s");
		//$rows_cid['datetime'] = $timestamp;
		$data = array(
			"from_user_cid" => $rows_cid['from_cid']['c_id'],
			"to_user_cid" => $rows_cid['your_cid']['c_id'],
			"datetime"	=> $timestamp,
			"message"	=>	$message,
			"seen"	=> 0
		);
		$this->Post_model->sendmessage($data);
		var_dump($data);
	}
	public function apply(){
		
		$cid = $this->Post_model->givecid();
		
		switch($this->input->get("control")){
			case "apply" : {
				$id = $this->input->get("id");			
				
				$data = array(
					"post_id"	=>	$id,
					"c_id"	=>	$cid[0]['c_id']
				);
				$this->Post_model->applyit($data);
				break;
			}
			case "check" : {
				$data = array(
				"c_id"	=>	$cid[0]['c_id']
			);
				$posts = $this->Post_model->checkit($data);
				$p = json_encode($posts);
				echo $p;
				break;
			}
		}
		
	}
	public function response(){
		$res = "";
		$cid = $this->Post_model->givecid();
		$responser = $this->Post_model->post_responses($cid[0]['c_id']);
		$counter = 0;
		//$responses->append($info_applier['f_name']);
		//var_dump($responser);
		if(!empty($responser)){
			foreach($responser as $r_cid){
				$names = $this->Post_model->get_credentials($r_cid['c_id']);
				//array_push($info_applier,$names[0]['f_name']);
				$info_applier[$counter]['name'] = $names[0]['f_name'];
				$counter++;
				//var_dump($names);
			}
			foreach($info_applier as $key => $val){
				$res .= '<a class="dropdown-item" href="#"><i>'.$val['name'].'</i> Applied to your post</a>';
			}
			
			echo $res;
		}
		else if(empty($responser)){
			$res .= '<li class="text-center">No Responses</li>';
			echo $res;
		}
	}
	public function load_post_request(){
		$current_user_info = $this->Login_model->return_current_user_info();
		$data['u_info'] = $current_user_info;
		$posts = $this->Post_model->get_post();
		$data['rows'] = $posts;
	
	
		$this->load->view('header',$data);
		$this->load->view('post_request',$data);
	}
	public function load_users(){
		$current_user_info = $this->Login_model->return_current_user_info();
		$data['u_info'] = $current_user_info;
		$users = $this->Login_model->users_load();
		$data['rows'] = $users;
		$this->load->view('header',$data);
		$this->load->view('usersload',$data);
		
	}
	public function blockuser(){
		$id = $this->input->get("id");
		$this->Login_model->block_unblock_user($id,1);
	}
	public function unblockuser(){
		$id = $this->input->get("id");
		$this->Login_model->block_unblock_user($id,0);
	}
}