<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('Register_model');
    }
	public function index()
	{
        //$this->load->library('form_validation');
        $this->load->view('register');
    }
    public function validate(){
        $this->form_validation->set_rules('fname','Name','required');
        $this->form_validation->set_rules('email','Email','required');
        $this->form_validation->set_rules('pass','Password','required');
        //$this->form_validate->set_rules('actype','Account Type','required');
        if($this->form_validation->run()){
            $enc_pass = md5($this->input->post('pass'));
            if($this->input->post('actype') == "ja"){
                $data = array(
                    'f_name'  =>   $this->input->post('fname'),
                    'email' =>   $this->input->post('email'),
                    'pass'  =>   $enc_pass,
                    'user_type' =>  $this->input->post('actype'),
                    'pro'   =>  0,
                    'blocked'   =>  0,
                    'post_count'    =>  0
                );
            }
            else{
                $data = array(
                    'f_name'  =>   $this->input->post('fname'),
                    'email' =>   $this->input->post('email'),
                    'pass'  =>   $enc_pass,
                    'user_type' =>  $this->input->post('actype'),
                    'pro'   =>  0,
                    'blocked'   =>  0,
                );
            }
            
            $id=$this->Register_model->insert($data);
            //echo $id;
            $this->load->view("login");
        }
        else{
            $this->index();
           //echo "problem";
        }
    }
}
