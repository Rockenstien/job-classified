<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_request extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('Post_request_model');
        $this->load->model('Post_model');
    }
    public function index()
	{
        if($this->lc->check()){
            $rows = $this->Post_request_model->request_posts();
            $data['rows']   =   $rows;
            $this->load->view('post_request',$data);
        }
        
    }
    public function approve(){
        $id = $this->input->get('id');
        $this->Post_request_model->approve($id);
            
        
    }
    public function reject_mail(){
        $to = $this->input->get("rec");
        $message = $this->input->get("message");
        $id = $this->input->get("id");
        $this->email->from('paras.klangyan@gmail.com', 'Admin Job-Classified');
        $this->email->to($to);
        $this->email->subject("Reject Post, read for the cause");
        $this->email->message($message);
        if($this->email->send()){
            echo "1";
            $this->Post_request_model->reject($id);
            //echo $this->email->print_debugger();
        }
        else{
            echo "0";
            //echo $this->email->print_debugger();
        }
    }
   
}
